export const state = () => ({
  products: [],
  cart: { 1: 2 },
})

export const getters = {
  getProductById: (state) => (id) => {
    return state.products[id - 1]
  },
  getProducts: (state) => {
    return state.products
  },
}

export const actions = {
  async fetchProducts(context) {
    if (context.state.products.length != 0) return
    const res = await this.$axios.$get(
      'https://6145add238339400175fc6b0.mockapi.io/products'
    )
    console.log(res)
    context.commit('setProducts', res)
  },
}

export const mutations = {
  setProducts(state, data) {
    state.products = data
  },
  addToCart(state, productId) {
    if (productId in state.cart) {
      state.cart[productId]++
    } else {
      state.cart[productId] = 1
    }
  },
}
